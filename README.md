# Adonis: Request Life Cycle

An Adonis application that mocks the CRUD operations


### Endpoints
GET: [Fetch all interns: http://127.0.0.1:3333/users](http://127.0.0.1:3333/users)

GET: [Fetch single intern: http://127.0.0.1:3333/users/:id](http://127.0.0.1:3333/users/1)

POST: [Create new user: http://127.0.0.1:3333/users](http://127.0.0.1:3333/users)

DELETE: [Delete single user: http://127.0.0.1:3333/users/:id](http://127.0.0.1:3333/users/1)

UPDATE [Update user: http://127.0.0.1:3333/users/:id](http://127.0.0.1:3333/users/1)

### Usage

```js
git clone https://gitlab.com/Developerkimaiyo/adonis-request-life-cycle

```

### Support

Reach out to me at one of the following places!

- Twitter at <a href="http://twitter.com/maxxmalakwen" target="_blank">`@maxxmalakwen`</a>

Let me know if you have any questions. Email me At maxwell@sendyit.com or developerkimaiyo@gmail.com

---

### License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2021 © <a href="https://github.com/Developer-Kimaiyo" target="_blank">Maxwell Kimaiyo</a>.
