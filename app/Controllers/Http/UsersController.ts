import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {
  schema,
  rules, // 👈 import validator
} from '@ioc:Adonis/Core/Validator'

export default class UsersController {
  private static users = [
    { id: 1, name: 'Maxwell Kimaiyo', email: 'maxwell@sendyit.com', stack: ['Vue', 'Go'] },
    { id: 2, name: 'Gill Erick', email: 'erick@sendyit.com', stack: ['Flutter', 'Quarkus'] },
    { id: 3, name: 'Stacy Chebet', email: 'stacy@sendyit.com', stack: ['Fluter', 'Adonis'] },
    { id: 4, name: 'Dorcas Cherono"', email: 'dorcas@sendyit.com', stack: ['Vue', 'Adonis'] },
  ]
  public async index() {
    return UsersController.users
  }



  public async store({ request }: HttpContextContract) {
    const data = request.only(['name', 'email', 'stack'])
    const newId = UsersController.users.length + 1
    const usersSchema = schema.create({
      name: schema.string(),
      email: schema.string({}, [rules.email()]),
      stack: schema.array().members(schema.string()),
    })


    
    await request.validate({ schema: usersSchema })
    const users = {
      id: newId,
      name: data.name,
      email: data.email,
      stack: data.stack,
    }
    UsersController.users.push(users)
    return users
  }
  public async show({ params }: HttpContextContract) {
    const userId = Number(params.id)
    return UsersController.users.find((p) => p.id === userId)
  }
  public async destroy({ params }: HttpContextContract) {
    const userId = Number(params.id) //transform to number
    return (UsersController.users = UsersController.users.filter((p) => p.id !== userId))
  }

  public async update({ params, request }: HttpContextContract) {
    const data = request.only(['name', 'email', 'stack'])
    const userId = Number(params.id) //transform to number
    const users = {
      id: userId,
      name: data.name,
      email: data.email,
      stack: data.stack,
    }
  UsersController.users  = UsersController.users.map((item) => {
  return item.id === userId ? users : item
})
return users
  }
}
